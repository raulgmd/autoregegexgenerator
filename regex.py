import re
import pdb
import logging


class ClassRegex:
  
  """
  loggin.basicConfig(filename="logFile.log",
                               format='%(asctime)s %(message)s',
                               filemode='w')
  
  logger=loggin.getLogger()
  
  logger.setLevel(logging.DEBUG)
  """
  
  """
  regexList= ["^[0-9]",  # numero  \d
                   "[^\w ]",  # carácter especial menos espacio (incluye ñ) \s
                   "^[A-Z]",  # mayuscula
                   "^[a-z]",  # minuscula
                   "^[A-z]",  # \w mayuscula o minuscula
                   "^[0-9a-zA-Z]", # mayuscula, minuscula o digito
                   "^[^A-Za-z0-9]"] # No es mayuscula, ni minuscula o digito
                   #"."]  # Cualquier carácter
   """
  regexList= ["[0-9]",  # numero  \d
                 "[\w ]",  # carácter especial menos espacio (incluye ñ) \s
                 "[A-Z]",  # mayuscula
                 "[a-z]",  # minuscula
                 "[A-z]",  # \w mayuscula o minuscula
                 "[0-9a-zA-Z]", # mayuscula, minuscula o digito
                 "[A-Za-z0-9]"] # No es mayuscula, ni minuscula o digito
                 #"."]  # Cualquier carácter
                   
  # staticRegex contiene una lista con cada una de las regex posibles
  staticRegex= []
  
  # dymanicRegex contiene una lista por cada letra
  # si todas las palabras tienen el mismo caracter en la misma posicion solo contiene esa caracter
  # de lo contrario contiene una lista para cada una de las opciones posibles en esa posicion
  dynamicRegex= []
  
  # Vector que indica la posicion actual de getReg en dymanicRegex
  dynamicRegexPosition= []
  
  currentRegex=0
  regexNumber=0
  staticRegexNumber=0
  maxIterations=10000
  
  
  # Calcula si la expresión regular concuerda con la 'position' de la palabra para las selecciones
  # indicadas
  # Devuelve el número de carácteres que concuerdan (PUEDE QUE SEA CONVENIENTE QUE AFECTE SOLO A 1)
  def isRegexPos(self,regex,position):
    
    """
      'Diferencia' almacena el número de carácteres que concuerdan
    """
    diferenciaAnterior=9999999999
    matched=False
    diferencia=0
    
    # Para cada una de las palabras del documento se comprueba si concuerda la regex
    # Recorremos las palabras
    for indice in self.selection:
      
      palabraAux=self.document[indice[0]:indice[1]]
      
      # Recortamos la palabra a la posición indicada
      palabra= palabraAux[position:]
      
      #pdb.set_trace()
      
      #Ejecutamos la expresión regular y comprobamos si al menos se selecciona 1 letra
      p2 = re.compile(regex)
      iterator=p2.finditer(palabra)

      #pdb.set_trace()

      iteradorVacio= True
      
      # Tenemos que comprobar que las palabras a las que corresponden la expresion
      # regular son todas las palabras seleccionadas
      for match in iterator:
        iteradorVacio= False
        #pdb.set_trace()
        
        # En diferencia almacenamos el numero de caracteres coincidentes entre 'palabra' y regex
        # Nos vamos quedando con la menor diferencia
        # Si una palabra no tiene coincidencias con la regex la diferencia debería ser 0
        diferencia= match.span()[1] - match.span()[0]
        
        if diferencia < diferenciaAnterior and match.span()[0]==0:
          diferenciaAnterior= diferencia
          matched=True

      # Si el iterador no tiene ninguna coincidencia para la palabra seleccionada
      # no hay ninguna coincidencia, la diferencia es 0 y devuelve 0
      if iteradorVacio:
        diferencia=0
        return 0
        #pdb.set_trace()
    
    if matched:
      
      return diferenciaAnterior
    else:
      return 0

    
    # Si la resta al final es almenos 1,
    # quiere decir que hay almenos 1 letra que coincide en

    """
      Casos a evaluar:
        Tenemos que comprobar que para todas las palabras seleccionadas, la regex coincide

        1. Obtenemos todas las palabras seleccionadas
        2. Obtenemos el número de letras que coincide por cada palabra
        3. Vamos restando el número de letras al de palabras
    """
    
    
    
    """
    # Position indica la posición actual
    # Regex indica la expresión regular
    # RegexAdd indica la expresion a añadir
    def generateRegex(self,position,regex,regexAdd):
      
      # Añade una expresión regular a una existente (carácter a carácter)
      regex= regex + "("+regexAdd+")"
      
      return regex
    """
  
  def generateDynamicRegex(self):
    
    # Calcula que tiene en común la selección y genera expresiones regulares
    # basadas en lo que tiene en común
    # Comprobamos si tiene texto igual y devolvemos una expresión regular que coincida
    isCaracter=False
    inicio=True
    #resultado=[]
    self.palabraInicial=''
    self.letrasComunes=[]
    regex=[]
        
    # ** Cambio **
    # A dynamicRegex sólo añadimos expresiones regulares completas
    # Recorremos las palabras seleccionadas para analizar lo común
    for indice in self.selection:
      
      palabra = self.document[indice[0]:indice[1]]
      
      if inicio:
        self.palabraInicial=palabra
      else:
        
        # Eliminamos lo que no sea comun
        i=0
        
        #for letra in self.palabraInicial:
        # Recorremos las letras de la palabra actual
        for letra in palabra:
          
          # Recorremos las letras de la palabra
          # Cuando se excede finalizamos el bucle
          if i>= len(palabra):
            break;
            
          # Para cada letra de la palabra en la posicion i
          # Obtiene las letras que comparte la seleccion que no se encuentran en lo no seleccionado
          if letra == self.palabraInicial[i]:
            if i not in self.letrasComunes:
            
              #pdb.set_trace()
              
              # Tenemos que incluir la letra en comun a currentRegex
              
              self.letrasComunes.append(i)
          else:
            # Se elimina de las letras comunes
            if i in self.letrasComunes:
              #pdb.set_trace()
              
              # Tenemos que incluir la regex coincidente en comun a currentRegex
            
              self.letrasComunes.remove(i)
          
          i=i+1
      
      inicio= False
    
    #pdb.set_trace()
    
    # En self.letrasComunes obtenemos -> [ 1 , 2 , 3 , 5 , 6 , 7 , 9 ]
    # Corresponde a la posición de las letras comunes de las 2 selecciones
    
    # Tenemos que construir la expresión regular basada en la posición
    # de las letras comunes
    
    i=0
    #regex=""
    regexMatrix= list()
    
    # Recorremos todas las letras de la palabra inicial
    for letra in self.palabraInicial:
      
      # Creamos una matriz
      regexMatrix.append(list())
      
      # Si la letra es de las comununes añadimos la letra a la expresión regular
      if i in self.letrasComunes:
        
        # Si la letra forma parte de las letras comunes la añadimos a la regex
        # Escapamos las letras
        letra = letra.replace('/','\/') #.replace('.','\.').replace('-','\-').replace('^','\^')
        
        regexMatrix[i].append(letra)
        #pdb.set_trace()
      
      # Incluimos las expresiones regulares válidas
      for regexStr_ in self.regexList:
        
        # Añadimos el caracter ^
        regexStr = '^'+regexStr_
        
        #pdb.set_trace()
        
        if self.isRegexPos(regexStr,i) > 0:
          
          #pdb.set_trace()
          
          # Eliminamos el carácter ^
          #if regexStr[0] == '^':
          #  regexStr == regexStr[1:]
          
          regexMatrix[i].append(regexStr_)
          
      # Incrementamos el contador dentro del bucle  
      i=i+1
      
    pdb.set_trace()
    
    # regexMatrix es una matriz que en cada fila almacena 
    # diferentes apariciones de la regex, en cada columna almacena
    # la regex para cada letra
    
    # Calculamos el número de filas y de columnas
    self.regexNumber= self.regexNumber+ (len(regexMatrix)**len(regexMatrix[0]))
    
    self.dynamicRegex= regexMatrix
    
    # A la hora de iterar por self.dynamicRegex hay que tener en cuenta que cada fila corresponde a 
    # una posición y cada columna corresponde con cada una de las posibles soluciones
    
    #pdb.set_trace()
    
  """
  # Generates RE depending of the selected words
  def generateStaticRegex(self):
    # fecha1
    # fecha2
    # fecha3
    # hora1
    # hora2
    # fecha y hora
    # DNI
    # Telefono
    # correo
    # mayusculas
    # alphanumerico
    # caracter especial
    # mayusculas y números
    startsUpperCase =r'\b([A-Z])(\S*?)\b'
    
    #resultadoDinamico= []
    
    #resultadoDinamico= self.dynamicRegex()
    
    #pdb.set_trace()
    
    # regex = re.compile(pattern, re.IGNORECASE)
    
    resultadoStatico =[startsUpperCase]
    
    return resultadoStatico
    
    #resultado= resultadoDinamico + resultadoStatico
    #resultado= resultadoDinamico + resultadoStatico
    
    return resultado
  """
  
  
  # Generates RE depending of the selected words
  def generateStaticRegex(self):
    # fecha1
    # fecha2
    # fecha3
    # hora1
    # hora2
    # fecha y hora
    # DNI
    # Telefono
    # correo
    # mayusculas
    # alphanumerico
    # caracter especial
    
    # Selecciona palabras entereas (caracteres separados de espacios)
    validDate = r'^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$m u'
    
    # mayusculas y números
    startsUpperCase =r'\b([A-Z])(\S*?)\b'
    
    # que la palabra contenga un caracter especial
    wordWithSpecialChar = r'(?:[_]\S+|\S*[^ A-Za-z0-9_]\S*)(?=\s|$)'
    
    # wordWithSpecialChar es posible crear una regex que seleccione palabras con una serie
    # de letras y sin otras letras, unaas plantillas de regex
    
    self.staticRegex = [startsUpperCase,wordWithSpecialChar]
    
    """
      regexList= ["^[0-9]",  # numero  \d
                   "[^\w ]",  # carácter especial menos espacio (incluye ñ) \s
                   "^[A-Z]",  # mayuscula
                   "^[a-z]",  # minuscula
                   "^[A-z]",  # \w mayuscula o minuscula
                   "^[0-9a-zA-Z]", # mayuscula, minuscula o digito
                   "^[^A-Za-z0-9]"] # No es mayuscula, ni minuscula o digito
    """
    
    #resultadoDinamico= []
    
    #resultadoDinamico= self.dynamicRegex()
    
    # regex = re.compile(pattern, re.IGNORECASE)
    
    # Almacenamos el número de regex
    
    #self.staticRegex =[startsUpperCase,specialChar]
    
    self.staticRegexNumber= len(self.staticRegex)
    
    self.regexNumber= self.regexNumber + self.staticRegexNumber
    
    #pdb.set_trace()
  
  
  
  # Incrementamos la posición para poder acceder a todas las permutaciones
  def nextDynamicRegexPosition(self):
    
    i=0
    incrementValue= True
    vectorSize = len(self.dynamicRegexPosition)
    
    #pdb.set_trace()
    # Realizamos el incremento de la posición actual de derecha a izquierda
    # teniendo en cuenta el número de elmentos que pueden ser referenciados
    # en cada posición del vector
    while i < vectorSize and incrementValue:
      
      # Incrementamos el contador de posicion
      i += 1
      
      # Accedemos primero al último elemento
      value = self.dynamicRegexPosition[vectorSize - i]
      
      #pdb.set_trace()
      
      # Obtenemos de dynamicRegex el número de elementos para realizar las permutaciones
      maxValue = len(self.dynamicRegex[vectorSize -i])
      
      #self.dynamicRegex
      if value+1 >= maxValue:
        self.dynamicRegexPosition[vectorSize - i] = 0
        incrementValue= True
        
      else:
        self.dynamicRegexPosition[vectorSize - i] += 1
        incrementValue= False
        
    # Devolvemos lo inverso a incrementValue,
    # de forma que si se había valores para sumarle 1 devuelve true, si no false
    return (not incrementValue)
  
  
  
  # Obtenemos la expresión regular basandonos
  # en el número actual
  # gexRegex tiene que ser capaz de componer una regex válida para dynamicRegex
  # y también devolver las regex estáticas
  def getRegex(self):
    #pdb.set_trace()
    dynamicRegexStatus= True;
    completeRegex=''
    
    # Primero devuelve las estáticas
    if self.currentRegex < self.staticRegexNumber and len(self.staticRegex) > 0:
      #pdb.set_trace()
      #print("static")
      aux =self.currentRegex
      self.currentRegex = self.currentRegex+1
      return self.staticRegex[aux]
      
    elif (len(self.dynamicRegex) > 0) and dynamicRegexStatus:
      
      # Si dynamicRegexPosition es lista vacía es la primera vez la inicializamos a 0 con el numero de columnas de dynamicREgex
      # Si es la primera vez que se entra en el bucle se inicializa a 0 el vector
      if len(self.dynamicRegexPosition) == 0:
        self.dynamicRegexPosition = [0] * len(self.dynamicRegex)
      
      #pdb.set_trace()
      
      i=0
      
      # Para generar la regex obtenemos las subregex utilizando la posicion indicada en dynamicRegexPosition
      for pos in self.dynamicRegexPosition:
      
        #pdb.set_trace()
        
        # Concatenamos las regex situadas en la posición indicada
        completeRegex += self.dynamicRegex[i][pos]
        
        # Incrementamos el contador que indica la fila
        i += 1
      
      # Avanzamos dynamicRegexPosition a la proxima posición
      dynamicRegexStatus = self.nextDynamicRegexPosition()
      
      # Una vez recorrido el vector, incrementamos el último valor
      #self.dynamicRegexPosition[i-1] += 1
        
      # Una vez obtenida la regex incrementamos el vector 'dynamicRegexPosition' (la ultima posicion)
      # 0 , 0 , 0 , 0, 0 , 1

      # incrementamos los contadores antiguos
      # Incrementamos el contador currentRegex (evaluar si es necesario)
      self.currentRegex = self.currentRegex+1
      
      #pdb.set_trace()
      
    # Devolvemos expresion regular completeRegex cuando existe y '' cuando no exista
    return completeRegex
  
  
  # Expresiones regulares dinámicas:
  # Empiezan por la mismas letras, terminan por las mismas, tienen las siguientes letras,
  # tienen antes o después las mismas letras, repetición de un carácter cada x posiciones
  def process_data(self):
    # Para el texto introducido probamos una serie de regex que se cumplan
    # en dichas cadenas y no se cumplan para el resto de las cadenas
    
    # Generamos las expresiones estaticas y las almacenamos en self.staticRegex ppp
    self.generateStaticRegex()
    
    # Generamos las expresiones dinámicas y la almacenamos en self.dynamicRegex
    # Contiene una lista por cada letra. En cada lista están las posibles opciones
    # de regex para cada posicion de las letras comunes
    self.generateDynamicRegex()
    
    #pdb.set_trace();
    
    # Obtenemos que restricción solo lo cumple la selección antes de la última
    # junto con el número de coincidencias después de la restricción
    
    reMatches=[]
    
    while self.currentRegex <= self.regexNumber and self.currentRegex<=self.maxIterations:
      
      #pdb.set_trace()
      # Obtenemos la expresion regular  - Se excede el número de filas y columnas
      
      # getRegex tiene que ser capaz de procesar bien dynamicRegex y staticRegex
      reg= self.getRegex()
      
      #pdb.set_trace();
      
      # Nos aseguramos de finalizar el bucle
      if reg== None or reg=="":
        i=self.maxIterations;
        #print("finalizando bucle: ") ppp
        #print(i)
        break;
      
      #pdb.set_trace()
      
      # obtenemos la palabra dentro del rango especificado
      matches = self.process_text(reg)
      
      #pdb.set_trace()
      
      # Si hay coincidencias quiere decir que ha coincidido con la 
      # selección y que ha encontrado coincidendias después de la selección
      if matches>0:
        #Añadimos a la lista la expresión regular junto con el número de coincidencias
        reMatches.append([reg,matches]);
      
      #pdb.set_trace() ppp
    
    maxRepetition=0
    maxre="";
    
    # Obtenemos la expresión regular con el mayor numero de coincidencias
    # Habría que modificarlo para obtener una lista de las regex con las mayores coincidencias
    for match in reMatches:
      if match[1] > maxRepetition:
        maxRepetition=match[1]
        maxre=match[0]
    
    # Ejecutamos la expresion regular dentro del texto
    p2 = re.compile(maxre)
    #match
    iterator=p2.finditer(self.document)
    
    finalList=list()
    
    for match in iterator:
      finalList.append(match.span())
    
    return finalList
  
  
  
  def process_text(self,reg):
    
    try:
      toSelectedText= self.document[0:self.selection[-1][-1]]
      afterSelectedText= self.document[self.selection[-1][-1]+1:]
      
      #pdb.set_trace()  # ppp
      
      p = re.compile(reg)
      iterator=p.finditer(toSelectedText)
      
      iteratorList=[]
      
      for match in iterator:
        iteratorList.append(match.span())
      
      reMatches=0
      
      # Si los iteradores de la expresión regular
      # coinciden con los iteradores de las palabras seleccionadas
      if iteratorList == self.selection:
        # Realizamos la búsqueda después de la selección
        p2 = re.compile(reg)
        iterator2=p2.finditer(afterSelectedText)
        
        # Calculates the number of items matched
        reMatches=len(list(iterator2))
        
      return reMatches
    except:
      print("Ha ocurrido una excepcion");
  
  
  def __init__(self, document, selection):
    self.document = document
    self.selection = selection
  
  
  def getDocument(self):
    return self.document
  
  
  def getSelection(self):
    return self.selection
  
  
  def setSelection(self,selection):
    self.selection= selection;
  
  
  def setDocument(self,document):
    self.document= document
  
  
