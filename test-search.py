import regex
import unittest
import pdb


class TestSearch(unittest.TestCase):
    # Run only 1 test:    python3 test-search.py TestSearch.test_DataProcess
    
    #############################################
    #### INICIO PRUEBA  PRUEBAS DINAMICAS  ####
    #############################################


    #############################################
    #### FIN PRUEBA  PRUEBAS DINAMICAS  ####
    #############################################
    
    
    # generateDynamicRegex test 1 pruebas de process data
    # Probamos los valores que se puedan validar con esta regex
    
    # generatedynamicregex obtene que tiene en comun la seleccion y lo va componiendo con
    # las regex prestablecidas, 
    
    # Se puede mejorar el sistema de ir adicionando letras con expresiones regulares añadiendo
    # regex con plantillas
    # Se define una clase dynamicRegex que permite operar con dichas regex y esta clase se
    # encarga de generar la regex basada en las letras comunes que se encuentran
    # Serian regex basadas en palabras y las regex tendrían para incluir las letras coincidentes y las no coincidentes
    """
      regexList= ["^[0-9]",  # numero  \d
                   "[^\w ]",  # carácter especial menos espacio (incluye ñ) \s
                   "^[A-Z]",  # mayuscula
                   "^[a-z]",  # minuscula
                   "^[A-z]",  # \w mayuscula o minuscula
                   "^[0-9a-zA-Z]", # mayuscula, minuscula o digito
                   "^[^A-Za-z0-9]"] # No es mayuscula, ni minuscula o digito
    """
    # Ahora mismo acepta estas expresiones unidas con letras en comun, por lo tanto son regex basadas en posionales
    # Debería de detectar fechas, dni, hora y patrones con posiciones establecidas
    # Para detectar patrones no basados en la posiciones utilizariamos otras regex que se les 
    # pasaría las letras en comun y estas clases generarian la regex personalizada
    # Procedemos a comprobar si detecta una fecha
    def test_DataProcess_fecha(self):
      
      # Test data process
      #document=  "Do^umAnt* de Pr¿eb?s MM2ntrañ ''m*s como ªcTUº"
      document=  "01/03/2021 6*32@Ña de 11/07/2001 MM2ntras ''m*s 18/09/2201 ªcTUº 19/03/2021"
      selection= [(0,10),(22,32)]
      
      # Solucion intentando ser implementada
      # Se tienen un conjunto de regex generadas analizando el texto
      # Se aplican combinaciones de esas regex
      
      reg= regex.ClassRegex(document,selection)
      
      result=reg.process_data()
      
      """
      
        Posibles soluciones: 
        [0-9][0-9]\/[0-9][0-9]\/2[0-9][0-9][0-9]
        
          
      """
      
      self.assertEqual(result,[(0,10),(22,32),(48,58),(65,75)])
      
      
    """
    # generateDynamicRegex test 1 pruebas de process data
    # Probamos los valores que se puedan validar con esta regex
    def test_DataProcess_d1(self):
      
      # Test data process
      #document=  "Do^umAnt* de Pr¿eb?s MM2ntrañ ''m*s como ªcTUº"
      document=  "Do^umAnt* de Pr¿eb?s MM2ntras ''m*s como ªcTUº"
      selection= [(0,9),(13,20)]
      
      reg= regex.ClassRegex(document,selection)
      
      result=reg.process_data()
      
      self.assertEqual(result,[(0,9),(13,20),(30,35),(41,46)])
      
      
      
    #############################################
    #### INICIO PRUEBA  PRUEBAS ESTATICAS  ####
    #############################################
    
    # Test data process 2
    #  Expresión regular que detecta las palabras que tienen carácteres especiales
    
    def test_DataProcess_specialchar(self):
      
      # Test data process
      document= "Document* de Prueb*s mientras Vem*s como acT*a"
      selection= [(0,9),(13,20)]
      
      reg= regex.ClassRegex(document,selection)
      
      result=reg.process_data()
      
      self.assertEqual(result,[(0,9),(13,20),(30,35),(41,46)])
      
    # Test data process 2
    #  Expresión regular que detecta las palabras que tienen carácteres especiales
    def test_DataProcess_specialchar_2(self):
      
      # Test data process
      #document=  "Do^umAnt* de Pr¿eb?s MM2ntrañ ''m*s como ªcTUº"
      document=  "Do^umAnt* de Pr¿eb?s MM2ntras ''m*s como ªcTUº"
      selection= [(0,9),(13,20)]
      
      reg= regex.ClassRegex(document,selection)
      
      result=reg.process_data()
      
      self.assertEqual(result,[(0,9),(13,20),(30,35),(41,46)])
      
      
    #############################################
    #### FIN PRUEBA  'specialChar'  ####
    #############################################



    ###############################################
    #### PROBAMOS isRegexPos                   ####
    ###############################################
    
    def test_isRegexPos_parcial1(self):

      document= "doaaetrb nos dondetra 3 de Aola"

      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # En las palabras 'doaaetrb' y 'dondetra' usando la regex 'dond' no se obtiene ninguna coincidencia
      result= reg.isRegexPos("dond",0)

      # No debería haber coincidencias ya que la expresión no se repite en las dos palabras seleccionadas
      
      self.assertEqual(result,0)
    


    def test_isRegexPos_parcial2(self):
      
      document= "doaaetrb nos dondetra 3 de Aola"
      
      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # En las palabras 'doaaetrb' y 'dondetra' usando la regex 'dond' no se obtiene ninguna coincidencia
      result= reg.isRegexPos("etrb",4)

      # No debería haber coincidencias ya que la expresión no se repite en las dos palabras seleccionadas
      self.assertEqual(result,0)



    def test_isRegexPos_parcial3(self):

      document= "doaaetrb nos dondetra 3 de Aola"

      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # En las palabras 'doaaetrb' y 'dondetra' usando la regex 'dond' no se obtiene ninguna coincidencia
      result= reg.isRegexPos("det",3)

      # No debería haber coincidencias ya que la expresión no se repite en las dos palabras seleccionadas
      
      self.assertEqual(result,0)


    def test_isRegexPos_parcial4(self):

      document= "doaaetrb nos dondetra 3 de Aola"

      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # En las palabras 'doaaetrb' y 'dondetra' usando la regex 'dond' no se obtiene ninguna coincidencia
      result= reg.isRegexPos("tra",5)

      # No debería haber coincidencias ya que la expresión no se repite en las dos palabras seleccionadas
      
      self.assertEqual(result,0)
    
    
    ## Test searchRegex_1
    ##   Prueba la funcionalidad isRegexPos que comprueba si la expresión regular
    ##   existe y devuelve el número de carácteres con los que coincide indicados
      
    ##  Tenemos que añadir mas casos de uso para tener claro su funcionalidad
    def test_isRegexPos_1(self):
      
      document= "doaretta nos donretta 3 de Aola"
      
      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # etra es una expresion regular
      result= reg.isRegexPos("retta",3)
      
      self.assertEqual(result,5)
    
    
    
    ## Test searchRegex_1n
    ##   Prueba la funcionalidad isRegexPos que comprueba si la expresión regular
    ##   existe y devuelve el número de carácteres con los que coincide indicados
      
    ##   Tenemos que añadir mas casos de uso para tener claro su funcionalidad
    def test_isRegexPos_1n(self):
      
      document= "doaaetra nos dondetta 3 de Aola"
      
      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # etra es una expresion regular
      result= reg.isRegexPos("setra",4)
      
      # Comprobamos que no se obtiene ninguna coincidencia     
      self.assertEqual(result,0)
    
    
    
    ## Test searchRegex_2
    ##  Prueba la funcionalidad isRegexPos que comprueba si la expresión regular
    ##  existe y devuelve el número de carácteres con los que coincide indicados
    def test_isRegexPos_2(self):
      
      document= "donaetra nos dondetra 3 de Aola"
      
      # Seleccionamos las palabras 'donaetra' y 'dondetra'
      selection= [[0,8],[13,20]]
      
      reg= regex.ClassRegex(document,selection)
      
      result= reg.isRegexPos("don",0)
      
      self.assertEqual(result,3)
    
    
    #Test searchRegex
    #  Prueba la funcionalidad isRegexPos que comprueba si la expresión regular
    #  existe y devuelve el número de carácteres con los que coincide indicados
    def test_isRegexPos_3(self):
      document= "Eola 3 de Aola"
      selection= [[0,4],[10,14]]

      reg= regex.ClassRegex(document,selection)
      
      result= reg.isRegexPos("la",2)
      
      self.assertEqual(result,2)
    
    
    
    # Test searchRegex
    #  Prueba la funcionalidad isRegexPos que comprueba si la expresión regular
    #  existe y devuelve el número de carácteres con los que coincide indicados
    def test_isRegexPos_4(self):
      document= "loaaetrb nos coaaetra 3 de Aola"

      selection= [[0,8],[13,21]]
      
      reg= regex.ClassRegex(document,selection)
      
      # etra es una expresion regular
      result= reg.isRegexPos("oaa",1)
      
      self.assertEqual(result,3)

    
    # Test searchRegex
    #  Prueba la funcionalidad isRegexPos que comprueba si la expresión regular
    #  existe y devuelve el número de carácteres con los que coincide indicados
    def test_isRegexPos_4n(self):
      document= "doaaetrb nos dondetra 3 de Aola"

      selection= [[0,8],[13,22]]
      
      reg= regex.ClassRegex(document,selection)
      
      result= reg.isRegexPos("oaa",0)
      
      # Comprobamos que no existe coincidencias para esa expresion regular en la
      # posición 0
      self.assertEqual(result,0)
    
    #############################################
    ####    FIN PRUEBA  isRegexPos           ####
    #############################################


    #############################################
    #### INICIO PRUEBA  'primera mayuscula'  ####
    #############################################

    # Test data process 2
    #  Expresión regular que detecta las palabras que comienzan por mayúsculas
    def test_DataProcess2(self):
        
        # Test data process
        document= "Documento de Pruebas mientras Vemos como acTua"
        selection= [(0,9),(13,20)]

        reg= regex.ClassRegex(document,selection)

        result=reg.process_data()

        self.assertEqual(result,[(0,9),(13,20),(30,35)])
    
    
    # Test process_data()
    #  Expresión regular que detecta las palabras que comienzan por mayúsculas
    def test_DataProcess(self):

        # Test data process
        document= "Documento de PruNbas mieNtras Vemos como actua B"

        # Se seleccionan las palabras 'Documento' y 'PrueNbas'
        selection= [(0,9),(13,20)]

        reg= regex.ClassRegex(document,selection)

        result=reg.process_data()

        # Comprobamos que las palabras seleccionadas son 'Documento' 'PruNbas' 'Vemos' y 'B'
        self.assertEqual(result,[(0,9),(13,20),(30,35),(47,48)])
    
        
    # Test process_data()
    #  Expresión regular que detecta las palabras que comienzan por mayúsculas
    def test_DataProcess_2(self):
        # Test data process
        document= "Documento de PruNbas mieNtras Vemos como actua Bes  "

        # Se seleccionan las palabras 'Documento' y 'PrueNbas'
        selection= [(0,9),(13,20)]

        reg= regex.ClassRegex(document,selection)

        result=reg.process_data()

        # Comprobamos que las palabras seleccionadas son 'Documento' 'PruNbas' 'Vemos' y 'Bes'
        self.assertEqual(result,[(0,9),(13,20),(30,35),(47,50)])


    #############################################
    ####    FIN PRUEBA  'primera mayuscula'  ####
    #############################################

    # Test getDocument()
    #  Prueba de creación y lectura del texto de un documento
    def test_Document(self):
        # Test getDocument
        document= "Documento de Pruebas mientras Vemos como actua"
        selection= [[13,19],[30,35]]

        reg= regex.ClassRegex(document,selection)

        self.assertEqual(reg.getDocument(),"Documento de Pruebas mientras Vemos como actua")

    
    # Test getSelection()
    #  Prueba de creación y lectura de una selección de un documento
    def test_selection(self):
        # Test get selection
        document= "Documento de Pruebas mientras VEIPS como actua"
        selection= [[13,19],[30,35]]

        reg= regex.ClassRegex(document,selection)

        self.assertEqual(reg.getSelection(),[[13,19],[30,35]])

"""


if __name__ == '__main__':
    unittest.main()

